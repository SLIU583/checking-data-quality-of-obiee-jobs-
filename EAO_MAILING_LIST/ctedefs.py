from cte import CTE
active_prep_job_cte = CTE("ACTION_PREP", [], '''
    select   EMPLID
            ,EMPL_RCD
            ,UW_JOB_START_DATE
            ,UW_JOB_END_DT
            ,POSITION_NBR
            ,EMPL_CLASS
            ,DEPTID
            ,JOBCODE
            ,BUSINESS_UNIT
            ,HR_STATUS
            ,UW_PAY_BASIS 
            ,rank() over (partition by EMPLID
                                    ,EMPL_RCD
                          order by EFFDT desc
                                    ,EFFSEQ desc
                                    ,COMP_EFFSEQ desc) as ACTION_RANK
        from SYSADM.PS_UW_HR_ALLJOB_VW
        where 
          HR_STATUS='A'
          and UW_JOB_START_DATE <  DATE '2019-07-01'  /* as of jobs */
          and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >=  DATE '2019-07-01')
''')

active_job_cte = CTE ("ALLJOB", [active_prep_job_cte], '''
    select   EMPLID
            ,EMPL_RCD
            ,substr(DEPTID,1,3) as DIVISION
            ,EMPL_CLASS
    from ACTION_PREP
    where ACTION_PREP.ACTION_RANK = 1 
    and BUSINESS_UNIT='UWMSN'
    and EMPL_CLASS in ('AS', 'FA', 'LI', 'CP', 'CJ', 'ET1', 'ET2', 'ET3', 'ET4')
    and UW_PAY_BASIS in ('C', 'A', 'H')
''')

active_prep_addr_job_cte = CTE("ACTION_ADDR_PREP", [], '''
    select  EMPLID, 
            EFFDT,
            ADDRESS_TYPE, 
            ADDRESS1, 
            ADDRESS2, 
            ADDRESS3, 
            CITY, 
            STATE, 
            POSTAL,
            rank() over (partition by EMPLID 
                          order by EFFDT desc) as ACTION_RANK
    from SYSADM.PS_UW_HR_ADDRES_VW
    where ADDRESS_TYPE = 'HOME' 
''')

active_addr_job_cte = CTE ("ADDRES", [active_prep_addr_job_cte], '''
    select  EMPLID, 
            ADDRESS_TYPE, 
            ADDRESS1, 
            ADDRESS2, 
            ADDRESS3,  
            CITY, 
            STATE, 
            POSTAL
    from ACTION_ADDR_PREP
    where ACTION_ADDR_PREP.ACTION_RANK = 1 
''')

active_person_job_cte = CTE ("PERSON", [], '''
    select   distinct EMPLID,
             FIRST_NAME, 
             LAST_NAME
    from SYSADM.PS_UW_HR_PERSON_VW
    where  NAME_TYPE = 'PRI'
           and trim(LAST_NAME) is not NULL
           and trim(LAST_NAME) <> '-'
''')

active_all_empl = CTE("JOIN_ALL_EMPL", [active_person_job_cte, 
                                        active_job_cte, 
                                        active_addr_job_cte], '''
    select
        ALLJOB.EMPLID,
        ALLJOB.EMPL_RCD,
        (case when ALLJOB.DIVISION in ('A46', 'A47') then 'EXTENSION'
              else 'MADISON'
        end) as MAILING_GROUP, 
        ALLJOB.DIVISION,
        ALLJOB.EMPL_CLASS,
        PERSON.FIRST_NAME,
        PERSON.LAST_NAME,
        ADDRES.ADDRESS1,
        ADDRES.ADDRESS2,
        ADDRES.ADDRESS3,
        ADDRES.CITY,
        ADDRES.STATE,
        ADDRES.POSTAL
    from ALLJOB 
    left join ADDRES
    on (ALLJOB.EMPLID = ADDRES.EMPLID)
    left join PERSON 
    on (ALLJOB.EMPLID = PERSON.EMPLID)
''')

active_empl_class_count_cte = CTE ("COUNT_EMPL_CLASS", [active_all_empl], '''
    select 
        MAILING_GROUP as mailingGroup, 
        EMPL_CLASS as emplClass,
        count(distinct EMPLID) as numOfEmpl 
    from JOIN_ALL_EMPL
    group by MAILING_GROUP, EMPL_CLASS
    order by MAILING_GROUP, EMPL_CLASS
''')

active_empl_division_count_cte = CTE ("COUNT_EMPL_DIVISION", [active_all_empl], '''
    select 
        MAILING_GROUP as mailingGroup, 
        DIVISION as division, 
        count(distinct EMPLID) as numOfEmpl 
    from JOIN_ALL_EMPL
    group by MAILING_GROUP, DIVISION
    order by MAILING_GROUP, DIVISION
''')

active_unique_madison_mailing_list = CTE("UNIQUE_MAILING_LIST", [active_all_empl], '''
    select distinct FIRST_NAME, 
                    LAST_NAME, 
                    ADDRESS1, 
                    ADDRESS2, 
                    ADDRESS3, 
                    CITY,
                    STATE,
                    POSTAL
    from JOIN_ALL_EMPL
    where MAILING_GROUP = 'MADISON' 
''')

active_unique_extension_mailing_list = CTE("UNIQUE_MAILING_LIST", [active_all_empl], '''
    select distinct FIRST_NAME, 
                    LAST_NAME, 
                    ADDRESS1, 
                    ADDRESS2, 
                    ADDRESS3, 
                    CITY,
                    STATE,
                    POSTAL
    from JOIN_ALL_EMPL
    where MAILING_GROUP = 'EXTENSION' 
''')

active_madison_mailing_list_empty_count = CTE("UNIQUE_MAILING_LIST", [active_all_empl], '''
    select count(distinct FIRST_NAME)
    from JOIN_ALL_EMPL
    where MAILING_GROUP = 'MADISON' 
    and 
''')