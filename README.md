# Checking Data Quality of OBIEE Jobs 

Assess the data quality of the OBIEE Jobs for a Given Data Range report versus a comparable query that we would write in EPM/SQL. 

We have some evidence from bug reports that the OBIEE report is producing unexpected results and would like to characterize the issues on a broader scale.

 

## Data set #1
OBIEE - Workforce Dashboards - **Jobs for a Given Date Range** - Jobs by Deptid, Empl Class and Jobcode
Parameters: 

              Business Unit = UW Madison

              Active Jobs From = 7/1/2018 To = 7/1/2018

              Empl Class = AS, FA, CL, CP, CJ, LI

Export as CSV to Excel

## Data set #2 
SQL shown as code 

## At the end of the project, achieve: 
1.  Bring both data sets into Python. 

2.  Remove columns in data set #1 that are not in data set #2.  After removing columns, remove duplicate rows.

3.  Compare the unique EMPLID's in between data sets. 

How many unique EMPLID's are there in each data set?  How many non-unique EMPLIDs?

How many EMPLID's are in common?  How many are in data set 1 but missing from data set 2?  How many are in data set 2 but missing from data set 1?

4.  Compare the unique combinations of EMPLID + EMPL_RCD.

How many unique EMPLID+EMPL_RCD are there in each data set?  How many non-unique EMPLID+EMPL_RCD in each data set?

How many are in common?  How many in data set 1 but not data set 2?  How many in data set 2 but not data set 1?

5.  For EMPLID + EMPL_RCD combinations in common,

Compare UW_JOB_START_DATE, UW_JOB_END_DT, POSITION_NBR, EMPL_CLASS, DEPTID, and JOBCODE.

For each of these data elements, how many are the same, how many in data set 1 but not data set 2, how many in data set 2 but not data set1?

 