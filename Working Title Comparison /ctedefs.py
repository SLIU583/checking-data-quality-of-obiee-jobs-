from cte import CTE
active_prep_job_cte = CTE("ACTION_PREP", [], '''
    select EMPLID
            ,EMPL_RCD
            ,UW_JOB_START_DATE
            ,UW_JOB_END_DT
            ,POSITION_NBR
            ,EMPL_CLASS
            ,DEPTID
            ,JOBCODE
            ,BUSINESS_UNIT 
            ,UW_WORKING_TITLE
            ,rank() over (partition by EMPLID
                                    ,EMPL_RCD
                          order by EFFDT desc
                                    ,EFFSEQ desc
                                    ,COMP_EFFSEQ desc) as ACTION_RANK
        from SYSADM.PS_UW_HR_ALLJOB_VW
        where 
          EFFDT <= DATE '2019-06-26' /* as of job actions */
          and UW_JOB_START_DATE <= DATE '2019-06-26' /* as of jobs */
          and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= DATE '2019-06-26')
''')

active_job_cte = CTE ("COUNT_EMPL", [active_prep_job_cte], '''
    select   EMPLID
            ,EMPL_RCD
            ,UW_WORKING_TITLE
    from ACTION_PREP
    where ACTION_PREP.ACTION_RANK = 1 
    and BUSINESS_UNIT='UWMSN'
    and substr(DEPTID, 1, 3)='A53'
''')

active_prep_job_cte2 = CTE("ACTION_PREP_2", [], '''
    select EMPLID
            ,EMPL_RCD
            ,UW_JOB_START_DATE
            ,UW_JOB_END_DT
            ,POSITION_NBR
            ,EMPL_CLASS
            ,DEPTID
            ,JOBCODE
            ,BUSINESS_UNIT 
            ,UW_WORKING_TITLE
            ,rank() over (partition by EMPLID
                                    ,EMPL_RCD
                          order by EFFDT desc
                                    ,EFFSEQ desc
                                    ,COMP_EFFSEQ desc) as ACTION_RANK
        from SYSADM.PS_UW_HR_ALLJOB_VW
        where 
          EFFDT <= DATE '2019-06-30' /* as of job actions */
          and UW_JOB_START_DATE <= DATE '2019-06-30' /* as of jobs */
          and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= DATE '2019-06-30')
''')

active_job_cte2 = CTE ("COUNT_EMPL_2", [active_prep_job_cte2], '''
    select   EMPLID
            ,EMPL_RCD
            ,UW_WORKING_TITLE
    from ACTION_PREP_2
    where ACTION_PREP_2.ACTION_RANK = 1 
    and BUSINESS_UNIT='UWMSN'
    and substr(DEPTID, 1, 3)='A53'
''')