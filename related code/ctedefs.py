# -*- coding: utf-8 -*-
"""
Created on Thu May 16 

@author: Lynn 
"""

from cte import CTE
active_prep_job_cte = CTE("ACTION_PREP", [], '''
    select EMPLID
            ,EMPL_RCD
            ,UW_JOB_START_DATE
            ,UW_JOB_END_DT
            ,POSITION_NBR
            ,EMPL_CLASS
            ,DEPTID
            ,JOBCODE
            ,BUSINESS_UNIT 
            ,rank() over (partition by EMPLID
                                    ,EMPL_RCD
                          order by EFFDT desc
                                    ,EFFSEQ desc
                                    ,COMP_EFFSEQ desc) as ACTION_RANK
        from SYSADM.PS_UW_HR_ALLJOB_VW
        where EFFDT <= CURRENT_DATE /* as of job actions */
          and  UW_JOB_START_DATE <= CURRENT_DATE /* as of jobs */
          and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= CURRENT_DATE)
''')
active_job_cte = CTE ("COUNT_EMPL", [active_prep_job_cte], '''
    select   EMPLID
            ,EMPL_RCD
            ,UW_JOB_START_DATE
            ,UW_JOB_END_DT
            ,POSITION_NBR
            ,EMPL_CLASS
            ,DEPTID
            ,JOBCODE
    from ACTION_PREP
    where ACTION_PREP.ACTION_RANK = 1 
    and BUSINESS_UNIT='UWMSN'
    and EMPL_CLASS in ('AS', 'FA', 'CL', 'CP', 'CJ', 'LI')
''')